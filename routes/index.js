const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const conn = require('../config/database');

const sendError = function (err, res) {
  switch (err.code) {
    case 422:
      res.status(422).json({error: err.message});
      break;
    case 401:
      res.status(401).json({error: err.message});
      break;
    default:
      res.status(500).json({error: err});
      break;
  }
};

// This route registers a user with the registration middleware in auth.js
router.post('/cat/register', passport.authenticate('register', {
  session: false,
  failWithError: true
}), 
async (req, res, next) => res.sendStatus(200),
async (err, req, res, next) => {
  sendError(err, res)
});


// This route logs in a user with the login middleware in auth.js
router.post('/cat/login', async (req, res, next) => {
  passport.authenticate('login', async (err, cat, info) => {
    try {
      if (err || !cat) {
        return sendError(info, res);
      }
      req.login(cat, { session: false }, async (error) => {
        if (error) return next(error)
        
        const body = { id: cat.id, username: cat.username };
        const token = jwt.sign({ user: body }, process.env.SECRET);

        // Update lastSeenAt each time a user logs in
        conn.query('UPDATE animal SET lastSeenAt = ? WHERE id = ?', [new Date(), cat.id],
          function (err, rows, fields) {
            if (err) return sendError(err, res);

            //Send back the token to the user
            return res.json({ token });
          }
        );
      });
    } catch (error) {
      return sendError(error, res);
    }
  })(req, res, next);
});

/* Returns all cats */
router.get('/cats', passport.authenticate('jwt', { session: false }),
  function (req, res) {
    const permittedParams = ['id', 'name', 'username'];
    const hasQueryParams = !!Object.keys(req.query).length;
    const isParamsValid = Object.keys(req.query).some(p => permittedParams.indexOf(p) >= 0);

    if (hasQueryParams && !isParamsValid) {
      return sendError({ code: 422, message: "invaild search critera"}, res);
    }

    let sql = 'SELECT birthdate, breed, username, id, imageUrl, name FROM animal';

    const queryParams = permittedParams.filter(field => req.query[field]);

    if(queryParams.length) {
      sql += " WHERE ";
      sql += queryParams.map(field => `${field} = ?`).join(" AND ");
    }
    conn.query(
      sql, 
      queryParams.map(field => req.query[field]),
      (err, rows) => {
        if (err) return sendError(err, res);
        res.json(rows);
    });
  }
);

/* Returns a random cat */
router.get('/cats/random', passport.authenticate('jwt', { session: false }),
  function (req, res) {
    conn.query('SELECT imageUrl, name, breed FROM animal ORDER BY RAND() LIMIT 1', (err, rows) => {
      if (err) return sendError(err, res);

      res.json(rows[0]);
    });
  }
);

module.exports = router;