const app = require('../app');
const express = require('express');
const request = require('supertest');
const agent = request.agent(app)
const conn = require('../config/database');
const Animal = require('../models/animal');

const cleanUpTable = () => {
  conn.query('TRUNCATE TABLE animal;', (err, rows) => {
    if (err) throw err;
  });
}

const animal = new Animal({
  username: 'murphTheStray',
  password: 'password',
  birthDate: new Date().toISOString(),
  breed: 'torie',
  imageUrl: 'http://cats.com',
  name: 'murphy',
  weight: '6.3',
});

describe('Animal', function () {
  describe('POST /cat/register', function () {
    beforeEach(function() {
      this.formData = { ...animal } 
      agent.set('Content-Type', 'application/x-www-form-urlencoded')
    });
    
    it('responds with password too short error', function (done) {
      this.formData.password = 'pass';
      agent
        .post('/cat/register')
        .send(this.formData)
        .expect(422, '{"error":"password < 8 characters"}', done);
    })

    it('responds with name missing error', function (done) {
      this.formData.name = undefined;
      agent
        .post('/cat/register')
        .send(this.formData)
        .expect(422, '{"error":"name missing"}', done);
    })

    it('responds with username invalid with a non-unique username', function (done) {
      agent
        .post('/cat/register')
        .send(this.formData)
        .expect(200).then(response => {

          agent
            .post('/cat/register')
            .send(this.formData)
            .expect(422, '{"error":"username invalid"}', done);
        })
    })

    it('responds with success', function (done) {
      agent
        .post('/cat/register')
        .send(this.formData)
        .expect(200)
        .end(done)
    })

    afterEach(function (done) {
      cleanUpTable();
      done();
    });
  });

  describe('POST /cat/login', function () {
    beforeEach(function (done) {
      this.formData = { ...animal }
      agent.set('Content-Type', 'application/x-www-form-urlencoded')
      agent.post('/cat/register')
        .send(this.formData)
        .expect(200).then(response => {
          done();
        });
    });

    it('responds with success', function (done) {
      agent
        .post('/cat/login')
        .send({
          username: animal.username,
          password: animal.password
        })
        .expect(200).then(response => {
          expect(response.body.hasOwnProperty('token')).toBe(true);
          done();
        })
    })

    it('responds with 401 unauthorized when the username is invalid', function (done) {
      agent
        .post('/cat/login')
        .send({
          username: 'invalid_user_name',
          password: animal.password
        })
        .expect(401, '{"error":"username or password invalid"}', done);
    })

    it('responds with 401 unauthorized when the username is invalid', function (done) {
        agent
          .post('/cat/login')
          .send({
            username: animal.username,
            password: 'invalid_password'
          })
          .expect(401, '{"error":"username or password invalid"}', done);
    })

    afterEach(function (done) {
      cleanUpTable();
      done();
    });
  });

  describe('get requests', function() {
    beforeAll(function (done) {
      cleanUpTable();
      this.formData = { ...animal }

      agent.set('Content-Type', 'application/x-www-form-urlencoded')
      agent
        .post('/cat/register')
        .send(this.formData)
        .expect(200).then(response => {
          agent
            .post('/cat/login')
            .send({
              username: this.formData.username,
              password: this.formData.password
            })
            .expect(200).then(response => {
              this.token = response.body.token
              done();
            })
        })
    });

    afterAll(function() {
      cleanUpTable();
    });

    describe('GET /cats', function () {
      it('responds with success', function (done) {
        agent
          .get('/cats')
          .set('Authorization', 'Bearer ' + this.token)
          .expect(200)
          .then(response => {
            expect(response.body[0].name).toEqual(animal.name);
            done();
          })
      })

      it('responds with 401 Unauthorized when token is invalid', function (done) {
        agent
          .get('/cats')
          .set('Authorization', 'Bearer ' + 'invalid_token')
          .expect(401)
          .end(done)
      })

      it('responds with success when searching by id', function (done) {
        agent
          .get('/cats?id=1')
          .set('Authorization', 'Bearer ' + this.token)
          .expect(200).then(response => {
            expect(response.body[0].name).toEqual(animal.name);
            done();
          })
      })

      it('responds with success when searching by name', function (done) {
        agent
          .get(`/cats?name=${animal.name}`)
          .set('Authorization', 'Bearer ' + this.token)
          .expect(200).then(response => {
            expect(response.body[0].name).toEqual(animal.name);
            done();
          })
      })

      it('responds with success when searching by username', function (done) {
        agent
          .get(`/cats?username=${animal.username}`)
          .set('Authorization', 'Bearer ' + this.token)
          .expect(200).then(response => {
            expect(response.body[0].name).toEqual(animal.name);
            done();
          })
      })

      it('responds with 422 when search is invalid', function (done) {
        agent
          .get(`/cats?invalid=${animal.username}`)
          .set('Authorization', 'Bearer ' + this.token)
          .expect(422, '{"error":"invaild search critera"}', done);
      })

      afterEach(function (done) {
        done();
      });
    });

    describe('GET /cats/random', function () {
      it('responds with success', function (done) {
        agent
          .get('/cats/random')
          .set('Authorization', 'Bearer ' + this.token)
          .expect(200)
          .then(response => {
            expect(response.body.name).toEqual(animal.name);
            done();
          })
      })

      it('responds with 401 Unauthorized when token is invalid', function (done) {
        agent
          .get('/cats/random')
          .set('Authorization', 'Bearer ' + 'invalid_token')
          .expect(401)
          .end(done)
      })

      afterEach(function (done) {
        done();
      });
    });

  });
});
