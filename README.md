## Deployed version of the app

The app has been deployed to: https://still-stream-86864.herokuapp.com

All of the endpoints are protected. You must pass a
Bearer Authorization Token with each request.

The expected content type header is: x-www-form-urlencoded.

#### Example registration:
```
curl -X POST \
  https://still-stream-86864.herokuapp.com/cat/register \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'username=bobTheCat&password=password&birthDate=1988-06-10T00%3A00%3A00.000Z&breed=tortie&imageUrl=https%3A%2F%2Fcats.com&name=prod_user&weight=6.5'
```
#### Example login:
```
curl -X POST \
  https://still-stream-86864.herokuapp.com/cat/login \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'username=bobTheCat&password=password'
```

#### Example cats:
```
curl -X GET \
  https://still-stream-86864.herokuapp.com/cats \
  -H 'Authorization: Bearer <YOUR TOKEN HERE>'
```

## Local Setup

### Database Setup
You will need to run the database setup script in order to create the database 
for the development and testing environments.

```
node ./scripts/database_setup.js
```

### Starting the web server
```
npm run dev
```
Visit: `http://localhost:4205`

### Running the tests
End-to-end test were written to test the api. The means nothing is mocked and the real endpoints are tested.
These type of specs have their place, but of course this is a big trade off with speed. In the real world I would start 
with unit specs since they execute much faster.

```
npm run test
```
