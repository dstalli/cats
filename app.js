const createError = require('http-errors');
const express = require('express');
const path = require('path');

if (process.env.NODE_ENV === 'test') {
  require('dotenv-override').config({path: '.env.test'});
}
else {
  require('dotenv-override').config()
}

require('./auth/auth');

const logger = require('morgan');
const cookieParser = require('cookie-parser');
const port = process.env.PORT || 4205;

const indexRouter = require('./routes/index');

const app = express();

app.listen(port);

app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

module.exports = app;
