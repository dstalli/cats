const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const conn = require('../config/database');
const Animal = require('../models/animal');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

//Create a passport middleware to handle animal registration
passport.use('register', new localStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true,
}, async (req, username, password, done) => {

  const cat = new Animal({ ...req.body })
  cat.birthDate = new Date(cat.birthDate);
  cat.lastSeenAt = new Date();
  cat.validate();
  
  if (cat.hasErrors()) {
    return done({code: cat.error.code, message: cat.error.message}, false);
  }

  try {
    bcrypt.hash(password, 10, function (err, hash) {
      cat.password = hash;
      cat.clearError();
      
      conn.query('INSERT INTO animal SET ?', cat, function (err, rows, fields) {
        if (err && err.code === "ER_DUP_ENTRY") {
          done({ code: 422, message: "username invalid" }, false);
        }
        else if(err) {
          throw err;
        }
        return done(null, {});
      });
    });
  } catch (error) {
    done(error);
  }
}));

//Create a passport middleware to handle User login
passport.use('login', new localStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true,
  session: false
}, async (req, username, password, done) => {

  conn.query("SELECT * FROM animal WHERE username = ?", [username], function (err, rows) {
    if (err) return done(err);
    if (!rows.length) {
      return done(null, false, { code: 401, message: "username or password invalid"});
    }

    if (!bcrypt.compareSync(password, rows[0].password.toString())) {
      return done(null, false, { code: 401, message: "username or password invalid" });
    }

    return done(null, rows[0]);
  });
}));

passport.use(new JWTstrategy({
  secretOrKey: process.env.SECRET,
  jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('Bearer')
}, async (token, done) => {
  try {
    return done(null, token.user);
  } catch (error) {
    done(error);
  }
}));