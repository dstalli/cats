const mysql = require('mysql');
const path = require('path');

const dbSetUp = () => {
  const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    insecureAuth: true
  });
  
  connection.query('CREATE DATABASE IF NOT EXISTS ' + process.env.DB_NAME );
  
  const sql = 'CREATE TABLE IF NOT EXISTS `' + process.env.DB_NAME + '`.`animal` ( \
      `id` INT(11) NOT NULL AUTO_INCREMENT, \
      `username` VARCHAR(30) NOT NULL, \
      `password` BINARY(60) NOT NULL, \
      `birthdate` DATE, \
      `breed` VARCHAR(30), \
      `imageUrl` TEXT, \
      `name` VARCHAR(30) NOT NULL, \
      `weight` FLOAT  NOT NULL, \
      `addedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(), \
      `lastSeenAt` DATETIME NOT NULL, \
      UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
      UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
  )';
  
  connection.query(sql);
  
  console.log('Database all set up!');
  connection.end();
}

const testEnvPath = path.join(__dirname, '..', '.env.test');
require('dotenv-override').config()
console.log(`Setting up ${process.env.DB_NAME}...`);
dbSetUp()
require('dotenv-override').config({ path: testEnvPath, override: true });
console.log(`Setting up ${process.env.DB_NAME}...`);
dbSetUp()
console.log(`All finished setting up databases!`);
