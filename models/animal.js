class Animal {
  constructor(props) {
    this.username = props.username;
    this.password = props.password;
    this.birthDate = props.birthDate;
    this.breed = props.breed;
    this.imageUrl = props.imageUrl;
    this.weight = props.weight;
    this.name = props.name;
    this.lastSeenAt = props.lastSeenAt;
    this.error = {};
  }

  hasErrors() {
    return !!Object.keys(this.error).length;
  }

  validate() {
    if(!this.name) {
      this.error = { code: 422, message: "name missing" };
    }
    else if (!this.password || this.password.length < 8) {
      this.error = { code: 422, message: "password < 8 characters" };
    }
  }

  clearError() {
    delete this.error;
  }
};

module.exports = Animal;